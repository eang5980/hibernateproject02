package org.dxc.factorydesign;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateFactory {

	public static final SessionFactory factory;
	
	static {
		factory = new Configuration().configure("resource/hibernate.cfg.xml").buildSessionFactory();
	}
	
	public static SessionFactory getFactoryObject() {
		return factory;
	}
	
}
