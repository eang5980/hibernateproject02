package org.dxc.factorydesign;

import org.dxc.Bean.Student;
import org.dxc.service.StudentService;
import org.dxc.service.StudentServiceImplementation;

public class ServiceFactory {

	public static final StudentService service;
	
	static {
		service = new StudentServiceImplementation();
	}
	
	public static StudentService getServiceObject() {
		return service;
	}
	
}
