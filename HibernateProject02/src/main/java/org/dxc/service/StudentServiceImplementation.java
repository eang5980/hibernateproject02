package org.dxc.service;


import java.util.List;

import org.dxc.Bean.Student;
import org.dxc.factorydesign.HibernateFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class StudentServiceImplementation implements StudentService {

	public Integer Insert(Student s) {
		// TODO Auto-generated method stub
		Integer sId;
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		sId = (Integer) session.save(s);
		tx.commit();
		return sId;
	}

	public List getAllStudent() {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		List studentList = session.createQuery("FROM Student").list();
		
		tx.commit();

		return studentList;

	}

	public Student getById(int id) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();

		Student student = session.get(Student.class, id);

		tx.commit();

		return student;

	}

	public void updateRecord(Student s) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Student student = session.get(Student.class, s.getId());
		
		student.setsName(s.getsName());
		student.setsAddr(s.getsAddr());
		session.update(student);
		tx.commit();
	}

	public void deleteRecord(int id) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateFactory.getFactoryObject();
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Student student = session.get(Student.class, id);
		session.delete(student);
		tx.commit();
		
	}

}
