package org.dxc.service;

import java.util.List;

import org.dxc.Bean.Student;

public interface StudentService {

	Integer Insert(Student s);
	List getAllStudent();
	Student getById(int id);
	void updateRecord(Student s);
	void deleteRecord(int id);
	
	
}
