package org.dxc.Bean;

import javax.persistence.*;


@Entity
@Table(name = "STUDENT")
public class Student {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String sName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public String getsAddr() {
		return sAddr;
	}

	public void setsAddr(String sAddr) {
		this.sAddr = sAddr;
	}

	@Column(name = "address")
	private String sAddr;

	public Student() {
	}

	public Student(String sName, String sAddr) {
		this.sName = sName;
		this.sAddr = sAddr;
	}
	

	

}
