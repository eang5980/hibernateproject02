package org.dxc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.dxc.Bean.Student;
import org.dxc.factorydesign.ServiceFactory;
import org.dxc.service.StudentService;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws IOException {
		int number;
		int sId;

		String sName;
		String sAddr;
		System.out.println("Press 1-> To insert StudentRecord");
		System.out.println("Press 2-> To get Student Details");
		System.out.println("Press 3-> To List Student Details");
		System.out.println("Press 4-> To Update Student Details");
		System.out.println("Press 5-> To delete Record");
		System.out.println("Press 6-> To Exit App");

		do {
			Scanner input = new Scanner(System.in);
			System.out.print("Enter choice:");
			number = input.nextInt();
			Student student = new Student();
			BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));
			StudentService service = ServiceFactory.getServiceObject();
			switch (number) {

			case 1:
				// Insert student information

				System.out.println("====================");
				System.out.println("Enter student name:");

				sName = buf.readLine();
				System.out.println("Enter student address:");
				sAddr = buf.readLine();

				student.setsName(sName);
				student.setsAddr(sAddr);
				try {
					System.out.println(
							service.Insert(student) != 0 ? "Student details inserted!" : "Insertion Uncessfull!");
				} catch (Exception e) {
					System.out.println("Something is wrong");
				}

				break;

			case 2:

				System.out.println("====================");
				System.out.println("Enter student id:");

				sId = input.nextInt();
				try {
					student = service.getById(sId);

					sName = student.getsName();
					sAddr = student.getsAddr();

					System.out.println("Student name: " + sName + "Address: " + sAddr);
				} catch (NullPointerException e) {
					System.out.println("ID not found!");
				} catch (Exception e) {
					System.out.println("Something is wrong");
				}
				break;

			case 3:
				try {
					List studentList = service.getAllStudent();
					System.out.println("Students List");
					System.out.println("===========================");

					for (Iterator iterator = studentList.iterator(); iterator.hasNext();) {
						student = (Student) iterator.next();
						System.out.print("Name: " + student.getsName());

						System.out.println("  Address: " + student.getsAddr());
					}
				} catch (NullPointerException e) {
					System.out.println("No records found! ");
				} catch (Exception e) {
					System.out.println("Something is wrong");
				}
				break;

			case 4:
				try {
					System.out.println("====================");
					System.out.println("Enter student id:");
					sId = input.nextInt();
					student = service.getById(sId);
					BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
					System.out.println("Student Name[old name:" + student.getsName() + "]:");
					System.out.println("(leave blank to remain unchange)");
					sName = b.readLine();
					if (sName == null || sName == "") {
						sName = student.getsName();
					}
					System.out.println("Student Address[old address:" + student.getsAddr() + "]:");

					System.out.println("(leave blank to remain unchange)");
					sAddr = b.readLine();
					if (sAddr == null || sAddr == "") {
						sAddr = student.getsAddr();
					}

					student.setsName(sName);
					student.setsAddr(sAddr);

					service.updateRecord(student);

					System.out.println("Successfully Updated!");
				} catch (NullPointerException e) {
					System.out.println("No records found! ");
				} catch (Exception e) {
					System.out.println("Something is wrong");
				}
				break;

			case 5:
				try {
					System.out.println("====================");
					System.out.println("Enter student id:");
					sId = input.nextInt();
					service.deleteRecord(sId);
					System.out.println("Student ID "+sId + " successfully deleted!");
				} catch (IllegalArgumentException e) {
					System.out.println("ID not found!");
				} catch (Exception e) {
					System.out.println("Something is wrong");
				}
				break;

			case 6:
				System.out.println("Exiting...");
				System.exit(0);
				break;

			default:
				System.out.println("Invalid choice, try again");
				continue;

			}
		} while (number < 1 || number > 6);
	}
}
